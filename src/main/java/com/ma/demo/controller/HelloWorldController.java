package com.ma.demo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloWorldController {

	@GetMapping("/greetings")
	public ResponseEntity HelloWorld() {
		
		return new  ResponseEntity("Hi from Jenkins", HttpStatus.OK);
	}
	
	
}
